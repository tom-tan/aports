# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=wacomtablet
pkgver=6.1.2
pkgrel=0
pkgdesc="GUI for Wacom Linux drivers that supports different button/pen layout profiles"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="xinput"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libwacom-dev
	libplasma-dev
	plasma5support-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	xf86-input-wacom-dev
	"
checkdepends="xvfb-run"
subpackages="$pkgname-lang $pkgname-doc"
_repo_url="https://invent.kde.org/system/wacomtablet.git"
source="https://download.kde.org/stable/plasma/$pkgver/wacomtablet-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run -a ctest --test-dir build --output-on-failure \
		-E "Test.KDED.DBusTabletService"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4d03efe7f384e9a2a8a0e6439b8189107f1f5ff17ffdc3c6c07c0c3ae3c4d899b54b4bb5bd2d8f27b53812e476e75259ac54592ea715ce26250ec4206bc067c5  wacomtablet-6.1.2.tar.xz
"
