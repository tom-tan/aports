# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=layer-shell-qt
pkgver=6.1.2
pkgrel=0
pkgdesc="Qt component to allow applications to make use of the Wayland wl-layer-shell protocol"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	qt6-qtdeclarative-dev
	qt6-qtwayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/plasma/layer-shell-qt.git"
source="https://download.kde.org/stable/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_TESTING=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
54e532dad1ed7db1e61bd5757ba501fe5e63ceb437ee61cf8c62a391b3ebc86cbf36782d13b1603f98d0edb5783391f7a04932100be3a7cb68d56b620808729e  layer-shell-qt-6.1.2.tar.xz
"
