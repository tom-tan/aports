# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.95.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x: nix crate
# riscv64: segfault when compiling syn
arch="all !s390x !riscv64"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	libssh2-dev
	mimalloc2-dev
	openssl-dev
	sqlite-dev
	"
options="net"
subpackages="$pkgname-doc $pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz
	system-deps.patch
	cargo-update.patch
	"

export LIBSSH2_SYS_USE_PKG_CONFIG=1  # use system libssh2

# the polars plugin takes too much memory to build on some platforms
case "$CARCH" in
	armhf|armv7|ppc64le|riscv64|x86) _exclude_opts="--exclude nu_plugin_polars" ;;
esac

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF

		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		mimalloc = { rustc-link-lib = ["mimalloc"] }
		rusqlite = { rustc-link-lib = ["sqlite3"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --workspace --release --frozen $_exclude_opts
}

check() {
	case "$CARCH" in
	arm*)
		cargo test --workspace --frozen $_exclude_opts -- \
			--skip plugin_persistence::plugin_process_exits_when_nushell_exits \
			--skip plugins::stream::echo_interactivity_on_slow_pipelines
		;;
	*)
		cargo test --workspace --frozen $_exclude_opts
		;;
	esac
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;

	install -Dm644 LICENSE \
		-t "$pkgdir"/usr/share/licenses/$pkgname/
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell=$pkgver-r$pkgrel"

	amove usr/bin/nu_plugin_*
}

sha512sums="
cb13a647858a4ccd9dac3b93f509c9f6f8138e7b6f46a3b2abe5df656a931215a5d42bf39904693e9b31cf502e15a86ca59df9e4af0ca5391773fda6385c7c46  nushell-0.95.0.tar.gz
3ebe0d34ef0c88c2cc9486827711feea5ce102d8e02bcbc009e62e05c0c4ba51674d6e18711e337fb36cef707c8a65df9a5e0cf8f7bef780070b328e14f7cac6  system-deps.patch
35b1c3f2ef2d3a4feaf7e2a4f0984fdfd5543ceecfbfddcc945c3e5dd8f59f4b2be333ec5d0a3c236368d4ccd02597dfbc40e088024483590bd4c97513fef009  cargo-update.patch
"
