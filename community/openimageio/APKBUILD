# Contributor: Mark Riedesel <mark@klowner.com>
# Contributor: Damian Kurek <starfire24680@gmail.com>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openimageio
pkgver=2.5.13.0
pkgrel=0
pkgdesc="Image I/O library supporting a multitude of image formats"
options="!check" # more than 10% of all tests fail
url="https://sites.google.com/site/openimageio/"
# s390x has missing dependency ptex-dev
arch="all !s390x"
license="Apache-2.0"
makedepends="cmake
	boost-dev
	bzip2-dev
	ffmpeg-dev
	fmt-dev
	freetype-dev
	giflib-dev
	hdf5-dev
	libheif-dev
	libraw-dev
	libwebp-dev
	mesa-dev
	onetbb-dev
	opencolorio-dev
	openexr-dev
	openjpeg-dev
	ptex-dev
	ptex-static
	python3-dev
	py3-pybind11-dev
	qt6-qtbase-dev
	robin-map
	samurai
	tiff-dev
	"
subpackages="py3-$pkgname:_python $pkgname-dev $pkgname-doc $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenImageIO/oiio/archive/v$pkgver.tar.gz"
builddir="$srcdir/OpenImageIO-$pkgver"

build() {
	local _py_version=$(python3 --version | cut -c 8-11)
	local _iv="ON"

	case "$CARCH" in
		aarch64|armv7|armhf)
			_iv="OFF";;
	esac

	# fails to build with fortify source enabled
	export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"

	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_RPATH=ON \
		-DBUILD_TESTING=OFF \
		-DSTOP_ON_WARNING=OFF \
		-DENABLE_iv=$_iv \
		-DINSTALL_FONTS=OFF
	cmake --build build
}

check() {
	cd build
	ctest -E broken
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

tools() {
	pkgdesc="Tools for manipulating a multitude of image formats"

	amove usr/bin
}

_python() {
	pkgdesc="Python bindings for OpenImageIO image I/O library"

	amove usr/lib/python*
}

sha512sums="
dbab8612c15e38bd2b82ac9fc70d3d207afcb0746bf781c702acf1dd5f9e9e9d7e3b50517ab4e0aa767998a89636b83eb22632656bf0004c4f045280fa027a93  openimageio-2.5.13.0.tar.gz
"
