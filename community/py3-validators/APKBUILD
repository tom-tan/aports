# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=py3-validators
pkgver=0.30.0
pkgrel=0
pkgdesc="Python3 Data Validation for Humans"
url="https://github.com/python-validators/validators"
arch="noarch"
license="MIT"
depends="py3-decorator py3-pycryptodome py3-eth-hash"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-isort py3-pytest py3-flake8"
subpackages="$pkgname-pyc"
source="https://github.com/python-validators/validators/archive/$pkgver/py3-validators-$pkgver.tar.gz"
builddir="$srcdir/validators-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
c661327a7c15f7bdc9ac7af87c67ab71f39e35ea1a779e71f8d83d80a81a7e3138220acc24ffd65e41d22de0a71b4fab8e218988f55f152f70b7d3893e5c9246  py3-validators-0.30.0.tar.gz
"
