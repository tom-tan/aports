# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=haproxy-dataplaneapi
_pkgname=dataplaneapi
pkgver=2.9.4
pkgrel=0
pkgdesc="HAProxy data plane API"
url="https://github.com/haproxytech/dataplaneapi"
arch="all"
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/haproxytech/dataplaneapi/archive/refs/tags/v$pkgver.tar.gz
	haproxy-dataplaneapi.initd
	haproxy-dataplaneapi.confd
	dataplaneapi.yml
	"
options="net"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	export LD_FLAGS="\
		-X main.Version=$pkgver \
		-X main.GitTag=$pkgver \
		-X main.GitCommit=release \
		-X main.GitDirty= \
		-X main.GitRepo=$url \
		-X main.BuildTime=$(date -u '+%Y-%m-%dT%H:%M:%SZ') \
		"
	go build -ldflags="${LD_FLAGS}" -o dataplaneapi $builddir/cmd/dataplaneapi/main.go
}

check() {
	# TestReloadAgentDoesntMissReloads needs systemd
	go test -skip TestReloadAgentDoesntMissReloads ./...
}

package() {
	install -m755 -D "$builddir"/dataplaneapi \
		"$pkgdir"/usr/sbin/dataplaneapi
	install -m644 -D "$srcdir"/dataplaneapi.yml \
		"$pkgdir"/etc/haproxy/dataplaneapi.yml
	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
f59395a151ba4bd9f9e626f60abcbf43b556be2ae76d75de3813b80314feed52c3978aefe47913053f398af4c8c8aa2a24b3b447cfae279abf90196ef3dfd5f2  dataplaneapi-2.9.4.tar.gz
e38d0566e19a0c664c78c86e5e90986e7cda214c0c306ad9095bf3d0e28a3f84ec4afcca237c961b0ccdca13679b66fe8c6fc24007c8506fd2fc5553e01122f8  haproxy-dataplaneapi.initd
aa9373b5c3f44ffacad77ef075fc196d944c893a044e6a79d58125ac7b4b7c8c90c4c8c70dd69ba884e56752382221af5e9968d6c9241f6740b20bb5294bfd77  haproxy-dataplaneapi.confd
368c6877f79a53f597e34f91029e9c4c8fa141046d284b0a79a641e09ad32072ab9cc2ae691f173786557fc220cf0fc7b6ec0eecc41d20fd2bcff689a56cf185  dataplaneapi.yml
"
